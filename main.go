package main

import (
	_ "embed"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
)

var (
	//go:embed templates/struct.template
	structTpl  string
	schemaFile = flag.String("schema", "schema.json", "Entity schema file")
	entityName = flag.String("entity", "Foo", "Name for the struct")

	templateFuncs = map[string]interface{}{
		"structFieldID": func(s string) string {
			return strings.ReplaceAll(strings.Title(s), "Id", "ID")
		},
		"structFieldType": structFieldType,
	}
)

func structFieldType(propName string, p SchemaProperty, t SchemaTopLevel) (string, error) {
	typeName, refType := p.ReferencedType(t)
	if refType != nil {
		return "*" + typeName, nil
	} else if p.Type == "string" {
		if p.Format == "date-time" {
			return "time.Time", nil
		}
		if p.Pattern == "^[0-9a-f]{32}$" {
			return "ShopwareID", nil
		}
		return "string", nil
	} else if p.Type == "boolean" {
		return "bool", nil
	} else if p.Type == "integer" {
		return p.Format, nil
	} else if p.Type == "number" {
		if p.Format == "float" {
			return "float64", nil
		}
		return "", fmt.Errorf("unsupported number format: %s", p.Format)
	} else if p.Type == "object" {
		return "map[string]interface{}", nil
	} else if p.Type == "array" {
		if p.Items == nil {
			return "", fmt.Errorf("type is array but has no items set")
		}
		itemsType, err := structFieldType(
			"",
			SchemaProperty{
				SchemaPropertyFields: *p.Items,
				Items:                nil,
			},
			t,
		)
		if err != nil {
			return "", err
		}
		return "[]" + itemsType, nil
	}
	return "interface{}", nil
}

type SchemaType struct {
	Properties map[string]SchemaProperty `json:"properties"`
}

type SchemaTopLevel struct {
	SchemaType
	Definitions map[string]SchemaType `json:"definitions"`
}

func (t SchemaTopLevel) Definition(typeName string) *SchemaType {
	if s, ok := t.Definitions[typeName]; ok {
		return &s
	} else {
		return nil
	}
}

type SchemaPropertyFields struct {
	Ref      string `json:"$ref"`
	Type     string `json:"type"`
	Pattern  string `json:"pattern"`
	Format   string `json:"format"`
	ReadOnly bool   `json:"readOnly"`
}

type SchemaProperty struct {
	SchemaPropertyFields
	Items *SchemaPropertyFields `json:"items"`
}

func (s SchemaProperty) ReferencedType(topLevel SchemaTopLevel) (string, *SchemaType) {
	if s.Ref == "" {
		return "", nil
	}
	refParts := strings.Split(s.Ref, "/")
	refPartsLen := len(refParts)
	if refPartsLen == 0 {
		return "", nil
	}
	typeName := refParts[refPartsLen-1]
	return typeName, topLevel.Definition(typeName)
}

func main() {
	flag.Parse()

	contents, err := ioutil.ReadFile(*schemaFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not read schema file: %v", err)
		os.Exit(2)
	}

	topLevel := SchemaTopLevel{}
	err = json.Unmarshal(contents, &topLevel)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not parse schema: %v", err)
		os.Exit(2)
	}

	tpl, err := template.New("struct").Funcs(templateFuncs).Parse(structTpl)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not parse templates: %v", err)
		os.Exit(2)
	}

	err = tpl.Execute(os.Stdout, map[string]interface{}{
		"EntityName": *entityName,
		"EntityType": topLevel,
		"TopLevel":   topLevel,
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "error executing template: %v", err)
		os.Exit(2)
	}

	for subtypeName, subtype := range topLevel.Definitions {
		err = tpl.Execute(os.Stdout, map[string]interface{}{
			"EntityName": subtypeName,
			"EntityType": subtype,
			"TopLevel":   topLevel,
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, "error executing template: %v", err)
			os.Exit(2)
		}
	}

}
